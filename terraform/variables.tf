#
# TERRAFORM AUTHENTICATION
#
variable "tenant_id" {
  type = string
}
variable "subscription_id" {
  type = string
}
variable "client_id" {
  type = string
}
variable "client_secret" {
  type = string
}
variable "port" {
  type = number
}
variable "image_tag" {
  type = string
}
variable "db_url" {
  type = string
}
variable "db_pwd" {
  type = string
}
variable "db_user" {
  type = string
}

